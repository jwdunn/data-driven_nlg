# Read Me
*Jarryd Dunn, DNNJAR001*

This data-driven Natural Language Generation (NLG) system was developed to compare the
differences in the qualilty of the utterances produced by NLG systems using a data-driven 
or template-based approach for entries from the [Wikipedia Person 
and animal dataset](https://drive.google.com/open?id=1TzcNdjZ0EsLh_rC1pBC7dU70jINcsVJd).
The paper corresponding to this system can be found [here]( https://www.dropbox.com/s/kokym7qlx0tfa36/ComparingData_drivenNLG_Final_NoCover.pdf?dl=0).

## Running the Generator
In order to run the generator the paths to the training and evaluation file must be set.
The training file contains entries from the Wikipedia Person and Animal data set that will
be used to train the Sentence Planner and encoder-decoder model for the linguistic realiser.
While the evaluation file contains the entries from the Wikipedia Person and Animal data 
set that will be used as input for the NLG system. These entries need not come from the
Wikipedia Person and Animal data set as long as they follow the same format.
For both files each entry is stored as a single line in the file.

The user may also change the parameters by editing the config.yml and data.yml file. 
The data.yml file contains configuration information for the encoder-decoder model. This
includes parameters such as the number of training iterations as well as the  required file
paths for the model and its meta-data.
The config.yml file is used to specify the following:
+ training_file_prefix: file prefix (path excluding extension) to the training data.
+ evaluation_file_prefix: file prefix to evaluation data.
+ input_file_prefix: file predix to file containing input MRs.
+ utterance_output_file: generated utterances are written to this file.
+ res_output_file: generated metrics are written to this file.
+ model_config_file: configuration file for training the encoder-decoder model.
+ source_vocab_size: size of the vocabulary for the input data.
+ target_vocab_size: size of the vocabulary for the ouput data.

### Outputs
The outputs from the system are printed to the screen as well as saved to a file pred.txt.
If the generate metrics option is selected then the following metrics will also be 
calculated, displayed and written to a file (default: res/metricResults.json) for each 
utterance generated.
+ Bleu Score
+ Word Error Rate (WER) Score
+ Rouge Scores (precision, recall and F1 measure)
    * Rouge-1
    * Rouge-2
    * Rouge-3
    * Rouge-4
    * Rouge-L
    * Rouge-W
+ Number of Missed Facts
+ Number of Added Facts
+ Number of facts in input MR

The results are written to the file as JSON objects where the keys are the metric measure 
and the values are the results. Each JSON object is written to one line in the file.

### Using an Average of Checkpoints
To run the model using a model constructed as the average of several models the average
model must first be created. This can be done from the command line using OpenNMT.
```
onmt-average-checkpoints \
    --model_dir model_directory \
    --output_dir average_model_directory \
    --max_count maximum_number_of_checkpoints_averaged
```
The path to the average model directory must then be added to the infer file
(inferModel.sh) by adding:\
`--checkpoint_path average_model_directory/model`\
Official documentation for onmt-average-checkpoints can be found 
[here](http://opennmt.net/OpenNMT-tf/inference.html).\
main.py can then be run as per normal.

## Unit Tests
Unit tests can be run by executing testRunner.py. \
Unit tests are run to test:
+ the delexicalisation of entries: Determining the slot-types and values and replacing
 slot-values in the text with the corresponding slot-type.
+ the sentence planner: Generating a sentence plan given an array of slot-types.
+ the meaning representations: Determining the slot-types in the input entry and replacing
 slot-types in the final produced utterance with slot-values.
 
## Directories
.   
| -- main.py \
| -- MeaningRepresentation.py  \
| -- SentencePlanner.py  \
| -- delexicaliseDataset.py  \
| -- tests  \
|  | -- datasetUnittest.py  \
|  | -- sentencePlannerUnittest.py  \
|  | -- meaningRepresentationUnittest.py \ 
|  | -- testRunner.py \
| -- datasets  \
|  | -- wiki_person.json \
|  | -- wiki_person_training.json  \
|  | -- wiki_person_validation.json \ 
|  | -- wiki_person_testing.json  \
|  | -- wiki_animal.json  \
|  | -- wiki_animal_training.json \ 
|  | -- wiki_animal_validation.json  \
|  | -- wiki_animal_testing.json  \
|  | -- wiki_person_survey.json  \
|  | -- src-vocab_person.txt  \
|  | -- tgt-vocab_person.txt  \
| -- res  \
|  | -- metricResults.json

## Artefacts Produced
Several files are produced by the sytem.
+ Delexicalise Data Set
    + filename_sentenceTokens.txt
    Each line contains the tokens extracted from one sentence. These correspond with the
    sentences in the filename_delex.txt file.
    + filename\_delex.txt
    Each line contains a sinle delexicalised sentence.
    + filename\_trainingTokens.txt
    Each line contains the tokens extracted from the reference text for an entry in the
    data set. This file is used to train the sentence planner.
+ Sentence Planner
	+ filename\_prob.txt
	This file contains the probabilities a token appearing after a single token or pair of
	tokens.
+ Linguistic Realiser
	+ src-vocab_person.txt and tgt-vocab_person.txt
	These file contain the words in the vocabularies for the encoder-decoder model.
	src-vocab_person.txt contains the vocabulary for the input file while tgt-vocab_person.txt
	contains the vocab for the labels.
    + metricResults.json
    This file contains the results from the evaluation metric for each of the entries.
    The results are stored as a JSON object with the metrics as the keys and the value is
    the metric score.

## Dependances
+ [OpenNMT](http://opennmt.net/OpenNMT-tf/installation.html)
+ [Tensorflow](https://www.tensorflow.org/install)
+ [NLTK](https://www.nltk.org/install.html)
+ [Py-Rouge](https://pypi.org/project/py-rouge/)
+ [Jiwer](https://pypi.org/project/jiwer/)
+ [unittest](https://docs.python.org/3/library/unittest.html)
+ [yaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
+ [json](https://docs.python.org/2/library/json.html)
