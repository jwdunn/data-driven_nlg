#Sentence planner
#Jarryd Dunn
#2019/06/18
import json
import random
import copy
from nltk.metrics import *


def rankDict(d):
    '''

    :param d: dictionary with comparable values
    :return: list containing (value, key) pairs ordered by value
    '''
    items = []
    for key, val in d.items():
        items.append((val,key))
    return sorted(items, reverse=True)


def choices(current, probDict, end_offset, name_offset, remaining):
    '''
    Print possible choices for next token.
    :param current: current token/n-gram of tokens
    :param prob: dictionary of probabilites
    :param end_offset: end token offset
    :param name_offset: name_id token offset
    :param remaining: tokens that still need to be placed
    '''
    possible = {}
    for token in remaining:
        if token in probDict[current][0].keys():
            possible[token]=probDict[current][0][token]
    items = rankDict(possible)[:4]
    print("_"*10+current+"_"*10)
    for p, token in items:
        if token == "<end>":
            p += end_offset
        elif token == 'Name_ID':
            p += name_offset
        print("%s : %.3f\t"%(token, p), end='')
    print()
    print(remaining)
    

def getFrequencies(text, n=1):
    """
    Determines which tokens follow the which and how often.
    returns a dictonary of where the keys are tokens and the values
    are a list of dictionaries (where the key is the next word and the value is
    a the number of times) and the total number of words that follow the key token.
    n --> the number of tokens to form a key
    :param text: string of text
    :param n: int the maximum size of the n-grams
    :return: dictionary containing frequencies for n-grams
    """
    tokens = {}
    for i in range(0,len(text)-n):
        token = " ".join(text[i:i+n])
        nextToken = text[i+n]
        if token not in tokens.keys():
            tokens[token] = [{nextToken:1},1]
        elif nextToken in tokens[token][0].keys():
            tokens[token][0][nextToken] += 1
            tokens[token][1] += 1
        else:
            tokens[token][0][nextToken] = 1
            tokens[token][1] += 1
    return tokens


def calcProb(text, n=1):
    """
    Calculate the probability of following token/n-gram given the current
    :param text: A string of tokens
    :param n: max size of n-grams
    :return: Dictionary containing probabilities for the following n-gram given the current
    """
    tokens = getFrequencies(text,n)
    for i in range(n):
        tokens = {**tokens,**getFrequencies(text,i)} #merge dictionaries for tokens of different sizes
    for token in tokens.keys():
        for nextToken in tokens[token][0].keys():
            tokens[token][0][nextToken] = tokens[token][0][nextToken]/tokens[token][1]
    return tokens


def getSentencePlan(tokens, prob, start_prob):
    """
    Produce a sentence plan
    :param tokens: tokens to be placed in a sentence plan
    :param prob: probabilities of tokens following a token/n-gram of tokens
    :param start_prob: probabilities of tokens starting an utterance
    :return:
    """
    if len(tokens) == 0:
        return []
    if "<end>" not in tokens:
        tokens.append("<end>")
    end_offset = -1.15
    name_offset = -0.5
    remaining = copy.deepcopy(tokens)
    # Tokens required to be rendered
    inpt = tokens
    
    # Determine utterance start
    maxStart = -1
    startToken = ""
    for token in tokens:
        if token in start_prob.keys():
            if start_prob[token]>maxStart:
                maxStart = start_prob[token]
                startToken = token      
    sentencePlan = [startToken]
    if startToken != "Name_ID":
        remaining.remove(startToken)

    # Determine utterance body
    maxIter = 2*len(remaining)
    while len(set(remaining))>2 and maxIter>0:
        # print(remaining)
        maxP = -1
        nextToken = ""
        # print(len(sentencePlan))
        if len(sentencePlan)>1:
            current = sentencePlan[-2]+" "+sentencePlan[-1]
            # if len(remaining) < 5:
            #    choices(current, prob, end_offset, name_offset, remaining)
            if current in prob.keys():
                for key in remaining:
                    if key in prob[current][0].keys():
                        p = prob[current][0][key]
                        if key == '<end>':
                                p += end_offset
                        elif key == 'Name_ID':
                                p += name_offset
                        if key in remaining and p > maxP:
                            nextToken = key
                            maxP = p
            # print("Selected token: %s @ %.5f"%(nextToken, maxP))

        if nextToken=="" or maxP < 0:
            current = sentencePlan[-1]
            # choices(current, prob, end_offset, name_offset, remaining)
            if current in prob.keys():
                for key in set(remaining):
                    if key in prob[current][0].keys() and prob[current][0][key] > maxP:
                        p = prob[current][0][key]
                        if key == '<end>':
                                p += end_offset
                        elif key == 'Name_ID':
                                p += name_offset
                        if key in remaining and p > maxP:
                            nextToken = key
                            maxP = p
                    #print("New token (1) %s"%(nextToken))

        if (nextToken=="" or maxP<0) and len(set(remaining))>2:
            #nextToken="<end>"
            nextToken = random.choice(list(set(remaining).symmetric_difference(set(["Name_ID","<end>"]))))
                
        if nextToken not in ['','<end>',"Name_ID"]:
            remaining.remove(nextToken)  # if token found from remaining remove it
        
        if nextToken=="<end>":
            end_offset = -1.15
        elif nextToken=="Name_ID":
            name_offset = -1
        end_offset += 0.05
        name_offset += 0.1
        #print(nextToken, maxP)
        #print()
        sentencePlan.append(nextToken)
        maxIter -= 1

    if maxIter <=0:
        print("Remaining", remaining)
        print()
        return getSentencePlanRandom(tokens, prob, start_prob, 0.5)

    sentencePlan.append("<end>")
    
    return sentencePlan


def getSentencePlanRandom(tokens, prob, start_prob, random_prob = 0.6):
    """
    Generates a sentence plan
    :param tokens: Array of tokens
    :param prob: Dictionary containing token probabilities
    :param start_prob: Probability with which greedy selection is chosen
    :param random_prob: Probability with which the greedy sentence planner is chosen rather than the random plan
    :return: String containing tokens in sentence plan
    """

    # print("Tokens:",tokens)
    if len(tokens)==0:
        return []
    remaining = copy.deepcopy(tokens)
    #Tokens required to be rendered
    inpt = tokens
    #print("Iput tokens:\n",inpt)

    if "<end>" not in tokens:
        tokens.append("<end>")
    #Determine utterance start
    maxStart = -1
    startToken=""
    for token in tokens:
        if token in start_prob.keys():
            if start_prob[token]>maxStart:
                maxStart = start_prob[token]
                startToken = token      
    sentencePlan = [startToken]
    remaining.remove(startToken)

    #Determine utterance body
    maxIter = 2*len(remaining)
    while len(set(remaining))>2 and maxIter>0:
        maxP = -1
        nextToken = ""
        if random.random() < random_prob:
            if len(sentencePlan)>1:
                current = sentencePlan[-2]+" "+sentencePlan[-1]
                #print("Sentence plan", sentencePlan)
                #print("Remaining:",remaining)
                if current in prob.keys():
                    for key in prob[current][0].keys():
                        #print("Current token: %s, Key: %s"%(current,key))
                        if key in remaining and prob[current][0][key] > maxP:
                            nextToken = key
                            maxP = prob[current][0][key]
                            #print("New token (2) %s"%(nextToken))

            if nextToken=="":
                current = sentencePlan[-1]
                for key in prob[current][0].keys():
                    if key in remaining and prob[current][0][key] > maxP:
                        nextToken = key
                        maxP = prob[current][0][key]
                        #print("New token (1) %s"%(nextToken))
                    
            if nextToken!='' and nextToken!='Name_ID' and nextToken!='<end>':
                remaining.remove(nextToken) #if token found from remaining remove it 
            else:
                for key in prob[current][0].keys():
                    if key in inpt and prob[current][0][key] > maxP:
                        nextToken = key
                        maxP = prob[current][0][key]
                        #print("New token %s"%(nextToken))
        else:
            pTarget = random.random()
            pCurr = 0
            if len(sentencePlan)>1:
                current = sentencePlan[-2]+" "+sentencePlan[-1]
                if current in prob.keys():
                    for key in prob[current][0].keys():
                        if key in remaining and pTarget>pCurr:
                            nextToken = key
                            pCurr += prob[current][0][key]

            if nextToken=="":
                current = sentencePlan[-1]
                for key in prob[current][0].keys():
                    if key in remaining and pTarget>pCurr:
                        nextToken = key
                        pCurr += prob[current][0][key]
                        #print("New token (1) %s"%(nextToken))
                    
            if nextToken!='' and nextToken!='Name_ID' and nextToken!='<end>':
                remaining.remove(nextToken) #if token found from remaining remove it 
            else:
                for key in prob[current][0].keys():
                    if key in inpt and pTarget>pCurr:
                        nextToken = key
                        pCurr += prob[current][0][key]

                    
        if nextToken=="":
            nextToken="<end>"
            
        sentencePlan.append(nextToken)
        maxIter-=1

    if maxIter <=0:
        sentencePlan.append("<end>")
        return sentencePlan
    sentencePlan.append("<end>")
    
    return sentencePlan


def loadProb(filename):
    """
    Read in dictionary of probabilites
    :param filename: filename_prefix for file containing probabilities
    :return: dictionary with probabilities for tokens follow each other
    """
    
    with open("%s_prob.txt"%(filename),"r") as f:
        prob = json.loads(f.read())
        f.close()
        return prob
    
def loadFile(filename, n=1):
    """
    Read in file and generate probability dictionary
    :param filename: string containing the name of the file
    :param n: int maximum n-gram size
    :return: dictionary with probabilities for tokens follow each other
    """
    text=readTokens(filename)
    print("Number of tokens: %d"%(len(set(text))))
    prob = calcProb(text,n)
    prob_str =json.dumps(prob)
    # Calculate probabilities for next words
    with open("%s_prob.txt"%(filename),"w") as f:
        f.write(prob_str)
        f.close()
    return prob

def readTokens(filename):
    """
    Read in a file of tokens
    :param filename: filename prefix for token file
    :return: list of tokens
    """
    filename =  '%s_sentenceTokens.txt'%(filename) #Tokens taken from the dataset
    #Read input tokens from file
    with open(filename,"r") as f:
        text=f.read().split()
        f.close()
        return text

def readTokenLines(filename):
    """
    Read in tokens from a file
    :param filename: string prefix name of file containing tokens
    :return: 2D array containing arrays of tokens
    """
    filename =  '%s_sentenceTokens.txt'%(filename) #Tokens taken from the dataset
    #Read input tokens from file
    print("Reading tokens from %s"%(filename))
    with open(filename,"r") as f:
        text = f.readlines()
        for i in range(len(text)):
            text[i] = text[i].strip().split()[:-1]
        f.close()
        return text

def calcStartProb(text):
    """
    Returns dictionary containing the probabilities for different words starting a description.
    :param text: Array containing arrays of tokens
    :return probabilities for tokens starting a sentence
    """
    prob = {}
    #Calculate frequencies with wich a word starts a description
    for line in text:
        if line[0] in prob.keys():
            prob[line[0]]+=1
        else:
            prob[line[0]]=1

    #Calculate probability
    for k in prob.keys():
        prob[k]/=len(text)

    return prob

def testSentencePlan(text, prob, start_prob, numTests=1):
    """
    Tests the quality of the results produced by sentence planner.
    :param text: List containing lists of tokens forming sentences used to test sentence planner
    :param prob: Dictionary of probabilities for one n-gram following the current
    :param start_prob: Dictionary contining probabilities for n-grams starting sentences
    :param numTests: Number of tests to run
    :return a tuple containing arrays of the scores for recall, precision and accuracy
        as well as the number of failed generations.
    """
    options=[] #useful
    recallRes = []
    precisionRes = []
    accuracyRes = []
    fMeasureRes = []
    failed=0
    for i in range(numTests):
        try:
            index = random.randint(0,len(text)-1)
            #print(index) DEBUG
            options.append(text[index])
            inpt_set = set(text[index])
            #print("Reference:",text[index]) DEBUG
            res = getSentencePlan(text[index],prob,start_prob)
            if res == None:
                failed+=1
                continue
            #print() DEBUG
            #print("Result:",res) DEBUG
            if res!=None:
                res_set = set(res)
                recallRes.append(recall(inpt_set,res_set))
                precisionRes.append(precision(inpt_set,res_set))
                if len(inpt_set) == len(res_set):
                    accuracyRes.append(accuracy(inpt_set,res_set))
                else:
                    failed+=1
                fMeasureRes.append(f_measure(inpt_set,res_set))
        except ValueError as ve:
            print("FAILED %s\n"%(text[index]))
            print(ve)
            failed+=1
        #inducate progress
        if i%100 == 0:
            print("*",end="") #DEBUG
    print()
    return (recallRes,precisionRes,accuracyRes,fMeasureRes,failed)
        

if __name__=="__main__":
    prob={}
    # try to read from file otherwise n to load json_object from file
    dataset_name="datasets/wiki_person_training"
    text = readTokenLines("datasets/wiki_person_validation_1000")
    input_type = input("Read raw input from file? (y/n)")
    startProb = calcStartProb(text)
    if input_type=="y":
        prob = loadFile(dataset_name,2)
    else:
        prob = loadProb(dataset_name)

    tokens = ['Name_ID', 'country_of_citizenship', 'date_of_birth', 'sport']
    sentencePlan = getSentencePlan(tokens,prob,startProb)
    print("Input:",tokens)
    print("\nSentence Plan:\n"," ".join(sentencePlan))
    print() # Blanc line
    
    # Test plans produced
    numTests=len(text)
    testRes = testSentencePlan(text, prob, startProb, numTests)
    print("\nTest Results after %d tests:\n"%(numTests))
    if len(testRes[0]) > 0:
        print("Average Recall: %.3f"%(sum(testRes[0])/len(testRes[0])))
    if len(testRes[1]) > 0:
        print("Average Precision: %.3f"%(sum(testRes[1])/len(testRes[1])))
    if len(testRes[2]) > 0:
        print("Average Accuracy: %.3f"%(sum(testRes[2])/len(testRes[2])))
    if len(testRes[3]) > 0:
        print("Average f_measure: %.3f"%(sum(testRes[3])/len(testRes[3])))
    print("Failed: %d, %.3f percent"%(testRes[4], testRes[4]/len(testRes[1])))
