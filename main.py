# Main File for Data2Text Generation
import DelexciseData as data_proc
import json
import SentencePlanner
from nltk.translate.bleu_score import corpus_bleu, SmoothingFunction
import rouge
import os
import re
from jiwer import wer
from MeaningRepresentation import MeaningRepresentation
import yaml


def train_model(training_input, training_label, input_vocab=5000, label_vocab=5000, config_file='data.yml'):
    """
    Train OpenNMT encoder-decoder model
    :param training_input: string the name of the file containing the training data (tokens)
    :param training_label: string the name of the file containing the labels (english sentences)
    :param input_vocab: int the maximum size of the vocabulary for the training data
    :param label_vocab: int the maximum size of the vocabulary for the label data
    :param config_file: string file name for the OpenNMT config file
    """
    os.system("bash trainModel.sh %s %d %s %d %s"%(training_input, input_vocab, training_label, label_vocab, config_file))


def infer_model(inferenceFile, output_file):
    """
    Generates utterances using OpenNMT
    :param inferenceFile: string the name of the file containing the sentence plans to be realised.
    :param output_file: string the name of the file where the realised utterances are stored.
    """
    os.system("bash inferModel.sh %s %s"%(inferenceFile, output_file))


def rouge_score(text, reference, mode, max_n=4, length_limit=100):
    """
    Rouge metric implemented at: https://pypi.org/project/py-rouge/
    :param text: string Generated text
    :param reference: string Reference text
    :param mode: string mode for ROUGE
    :param max_n: int largest n-grams scored
    :param length_limit: maximum length compared
    """
    apply_avg = mode == 'Avg'
    apply_best = mode == 'Best'
    evaluator = rouge.Rouge(metrics=['rouge-n', 'rouge-l', 'rouge-w'], # Rouge calculated based on n-grams, longest common sub-sequence (LCS) and weighted LCS
                                max_n = max_n,
                                limit_length = True,
                                length_limit = length_limit,
                                length_limit_type='words',
                                apply_avg = apply_avg,
                                apply_best = apply_best,
                                alpha = 0.5,
                                weight_factor = 1.2,
                                stemming = True)
    scores = evaluator.get_scores(text, reference)
    return scores


def prepare_results(metric, p, r, f):
    """
    Format Rouge scores.
    Source: https://pypi.org/project/py-rouge/
    :param metric: rouge metric to use. One of rouge-n, rouge-l or rouge-w
    :param p: Precision value
    :param r: Recall value
    :param f: f1 value
    :return: string containing formatted values
    """
    return '\t{}:\t{}: {:5.2f}\t{}: {:5.2f}\t{}: {:5.2f}'.format(metric, 'P', 100.0 * p, 'R', 100.0 * r, 'F1', 100.0 * f)


def proc_reference(meaning_representation):
    """
    Returns reference text with sentences containing no tokens removed
    :param meaning_representation: MeaningRepresentation object containing reference text
    :return: list of reference texts
    """
    text = []
    values = set([v for vals in meaning_representation.lookup.values() for v in vals])
    for line in meaning_representation.text:
        lineSet = set(line)
        if len(values.intersection(lineSet)) > 0:
            text.append(line)
    return text


def disp_rouge(text, reference, max_n=4, length_limit=100, summerise=False):
    """
    Display Average, Best and individual rouge scores.
    Source: https://pypi.org/project/py-rouge/
    :param text: list containing generated text to be tested
    :param reference: 2-D Array containing reference texts
    :param max_n: Size of largest n-grams to test against
    :param length_limit: maximum text length.
    :param summerise: boolean if True only results greater than 0 are displayed
    """
    for mode in ["Avg"]: #, "Best", "Individual" Only one reference and hypothesis
        scores = rouge_score(text, reference, mode, max_n, length_limit)
        apply_avg = mode == 'Avg'
        apply_best = mode == 'Best'
        print("Evalutation with {}".format(mode))
        res = []
        try:
            for metric, results in sorted(scores.items(), key=lambda x: x[0]):
                if not apply_avg and not apply_best:  # value is a type of list as we evaluate each summary vs each reference
                    for hypothesis_id, results_per_ref in enumerate(results):
                        nb_references = len(results_per_ref['p'])
                        for reference_id in range(nb_references):
                            if not summerise or (summerise and results_per_ref['r'][reference_id] > 0):
                                print('\tHypothesis #{} & Reference #{}: '.format(hypothesis_id, reference_id))
                                print('\t' + prepare_results(metric, results_per_ref['p'][reference_id],
                                                             results_per_ref['r'][reference_id],
                                                             results_per_ref['f'][reference_id]))
                    print()
                else:
                    res.append((metric, results['p'], results['r'], results['f']))
        except Exception as e:
            print("Failed to evaluate with %s\n"%(mode))
            if not summerise:
                print(e)
                (len(text), len(reference))
        return res


def bleu_score(text, references):
    """
    Determine the BLEU score
    :param text: generated text
    :param references: reference text
    :return: float BLEU score
    """
    sf = SmoothingFunction()
    # Prepare references
    for i in range(len(references)):
        references[i] = " ".join(references[i]).split()
    references = [references] * len(text)

    # Prepare hypotheses
    for i in range(len(text)):
        text[i] = text[i].split()

    #print("Text %d References %d"%(len(text), len(references)))
    return corpus_bleu(references, text, smoothing_function=sf.method2)


def err(text, ref_obj):
    """
    Determines the number of facts/tokens that have been incorrectly added or left out
    :param text: list of strings forming the generated text
    :param ref_obj: json object correspond to the generated text
    :return: 3 tuple containing the number of missing, added and the total number of facts
    """
    ref_text = []
    missed = 0
    added = 0
    total = 0
    checked = ['Name_ID','sex_or_gender','instance_of'] # Exclude Name_ID from count

    for line in text:
        ref_text += " "+line

    ref_text = ''.join(ref_text)
    ref_text = "".join(ref_text).split(" ")
    for i in range(len(ref_text)):
        if '_' in ref_text[i]:
            ref_text[i] = re.sub('_', ' ', ref_text[i])

    for word in ref_text:
        if word in ref_obj.lookup.keys() and word not in checked:
            ref_text_count = ref_text.count(word)
            ref_count = len(ref_obj.lookup[word])
            if ref_text_count > ref_count:
                added += ref_text_count -  ref_count
                #print("Added %s %d"%(word, ref_text_count -  ref_count))
            else:
                missed += ref_count - ref_text_count
                #print("Missed %s %d"%(word, ref_count - ref_text_count))
            checked.append(word)

    for key in ref_obj.lookup.keys():
        if key not in checked:
            missed += len(ref_obj.lookup[key])
            #print("Skipped %s %d"%(key,len(ref_obj.lookup[key])))
        if key not in ["Name_ID",'instance_of', 'sex_or_gender']:
            total += len(ref_obj.lookup[key])

    return (missed, added, total)


def formatSentence(s, meaningRepresentation):
    """
    Fixes capitalization and spacing in the sentence.
    :param s: list of words and punctuation forming a sentence
    :return: list containing the formatted sentence
    """
    if len(s) == 0:
        return ""

    first = s[0].split()
    first[0] = first[0].capitalize()
    formatted = [" ".join(first)]
    i = 1
    isFemale = False
    if 'sex or gender' in meaningRepresentation.obj.keys():
        isFemale = meaningRepresentation.obj['sex or gender'][0]['mainsnak'] == 'female'

    while i < len(s):

        if isFemale:
            if formatted[-1] == 'he':
                formatted[-1] == 'she'
            elif formatted[-1] == 'He':
                formatted[-1] = 'She'
            elif formatted[-1] == 'his':
                formatted[-1] = 'her'
            elif formatted[-1] == 'His':
                formatted[-1] = 'Her'
        if s[i] == "(":
            formatted.append(s[i]+s[i+1])
            i += 1
        elif s[i] in ").,?!":
            formatted[-1]+=s[i]
        else:
            formatted.append(s[i])

        i += 1
    if formatted[-1][-1] not in [".",',','?','!']:
        formatted[-1] += '.'

    return formatted


if __name__ == "__main__":
    # Read in training data from config file
    with open('config.yml', 'r') as f:
        config_data = yaml.load(f, yaml.FullLoader)
        f.close()

    training_file_prefix = config_data['training_file_prefix']
    eval_file_prefix = config_data['evaluation_file_prefix']
    input_file_prefix = config_data['input_file_prefix']
    utterance_out_file = config_data["utterance_output_file"]
    res_out_file = config_data["res_output_file"]

    # Update data config to ensure consistency
    with open('data.yml', 'r') as f:
        config_model = yaml.load(f, yaml.FullLoader)
        f.close()

    config_model["data"]["train_features_file"] = training_file_prefix + '_trainingTokens.txt'
    config_model["data"]["train_labels_file"] = training_file_prefix + '_delex.txt'
    config_model["data"]["eval_features_file"] = eval_file_prefix + '_trainingTokens.txt'
    config_model["data"]["eval_features_file"] = eval_file_prefix + '_delex.txt'

    with open('data.yml', 'w') as f:
        yaml.dump(config_model, f, sort_keys=True)
        f.close()



    do_delexcise = ""
    while do_delexcise != "y" and do_delexcise != "n":
        do_delexcise = input("Do you want to delexcalise the training data set? (y/n)")

    if do_delexcise == "y":
        delex_train = data_proc.DelexciseData(training_file_prefix)
        delex_train.delexcise()
    do_delexcise = ""

    while do_delexcise != "y" and do_delexcise != "n":
        do_delexcise = input("Do you want to delexcalise the evaluation data set? (y/n)")

    if do_delexcise == "y":
        delex_valid = data_proc.DelexciseData(eval_file_prefix)
        delex_valid.delexcise()

    # Train Model
    do_train_model = ''
    while do_train_model != "y" and do_train_model != "n":
        do_train_model = input("Do you want to train the model(e.g if no existing model)? (y/n)")

    if do_train_model == "y":
        train_model(training_file_prefix + "_trainingTokens.txt", eval_file_prefix + "_delex.txt",
                    config_data["source_vocab_size"], config_data["source_vocab_size"],
                    config_data["model_config_file"])

    # Get Predictions
    # Generate sentence plan
    text = SentencePlanner.readTokenLines(input_file_prefix)
    startProb = SentencePlanner.calcStartProb(text)

    do_calc_tokens=""
    while do_calc_tokens !="y" and do_calc_tokens != "n":
        do_calc_tokens = input("Calculate/recalculate sentence probabilities? (y/n)")

    if do_calc_tokens =="y":
        prob = SentencePlanner.loadFile(input_file_prefix, 2)
    else:
        prob = SentencePlanner.loadProb(input_file_prefix)

    # Read in evaluation objects
    with open(input_file_prefix + ".json", 'r') as f:
        json_eval_objects = []
        sentence_plans = []
        for line in f:
            json_eval_objects.append(MeaningRepresentation(json.loads(line)))
            sp = " ".join(SentencePlanner.getSentencePlanRandom(json_eval_objects[-1].tokens, prob, startProb, 0.5))
            sentence_plans.append(sp.split("<end>")[:-1])

    # Write out sentence plans
    with open(input_file_prefix + "_sentencePlans.txt", 'w') as f:
        for sentence_plan in sentence_plans:
            #print("Sentence Plan",sentence_plan)
            for line in sentence_plan:
                if line.strip()!="":
                    f.write(line+"<end>\n")
        f.close()

    # Generate utterance
    infer_model(input_file_prefix + "_sentencePlans.txt", utterance_out_file) # UNCOMMENT! COMMENTED TO SPEED UP TESTING

    # Read in realised sentences
    with open(utterance_out_file,'r') as f:
        pred_text = f.readlines()
        utterances = []
        f.close()
        for line in pred_text:
            utterances.append(line)

    # Delexcise utterance
    offset = 0
    counter = 0
    plain_texts = []
    facts = []
    for i in range(len(sentence_plans)):
        sentence_plan = sentence_plans[i]
        n = len(sentence_plan)
        utterance = utterances[offset:offset+n]
        plain_texts.append(utterance)
        facts.append(err(utterance,json_eval_objects[i]))
        offset+=n

    do_run_tests = ''
    while do_run_tests != "y" and do_run_tests != "n":
        do_run_tests = input("Calculate metric scores for utterances? (y/n)")

    # Display outputs and metrics
    metric_results = []
    for i in range(len(plain_texts)):

        mr = json_eval_objects[i]
        plain_text = plain_texts[i]

        # Print utterance
        for j in range(len(plain_text)):
            plain_text[j] = " ".join(formatSentence(mr.replaceTokens(plain_text[j].split()), mr))
        print(i, " ".join(plain_text))
        print()

        if do_run_tests == 'y':
            metric_res = dict()  # dictionary to score results for metrics run
            metric_res['id'] = i
            metric_res['text_length'] = len(plain_text)
            metric_res['ref_length'] = len(mr.text)
            metric_res['num_tokens'] = mr.numTokens()
            metric_res['num_unk_tokens'] = mr.numUniqueTokens()

            # Display facts included
            print("Facts From MR:\n Missed: %d\tAdded: %d\tExpected: %d\n" % (facts[i]))
            metric_res['missed'] = facts[i][0]
            metric_res['added'] = facts[i][1]
            metric_res['total'] = facts[i][2]

            reference_text = proc_reference(mr)
            reference_text=[" ".join([word for sent in reference_text for word in sent])]

            # Measure ROUGE Score
            print("ROUGE Tests")
            rouge_res = disp_rouge([" ".join(plain_text)],[reference_text], summerise=False)
            for res in rouge_res:
                print(prepare_results(*(res)))
                metric_res["%s-p" % (res[0])] = res[1]
                metric_res["%s-r" % (res[0])] = res[2]
                metric_res["%s-f" % (res[0])] = res[3]

            # Measure WER Score
            wer_res = wer(" ".join(plain_text),reference_text[0], standardize=True)
            metric_res['wer'] = wer_res
            print("\nWord Error Rate (WER): %.2f\n" % (wer_res))
            metric_results.append(metric_res)

            # Measure BLEU Score
            bleu_res = bleu_score(plain_text, mr.text)
            metric_res["bleu"] = bleu_res
            print("BLEU Tests")
            print("BLEU Score: %.3f\n" % (bleu_res))  # Align front
            # print("Bleu Score: %.3f" % (bleu_score(plain_text[len(plain_text)-len(mr.text):], mr.text))) # Align to back

            print("-" * 100)
            print()

    # Summerize results and write to file
    if do_run_tests == 'y':
        summary = {} # Summary of results where values take the form (total, count)
        for metric_res in  metric_results:
            for key in metric_res.keys():
                if key not in summary.keys():
                    summary[key] = (metric_res[key],1)
                else:
                    prev = summary[key]
                    summary[key] = (prev[0]+metric_res[key], prev[1]+1)

        summary['id'] = 'summary'
        metric_results.append(summary)

        print("Average Metric Scores:")
        for key in summary.keys():
            if key != 'id' and 'rouge' not in key:
                print("Average %s = %.3f"%(key, summary[key][0]/summary[key][1]))

        # Display average rouge results
        for mode in ['1', '2', '3', '4', 'l', 'w']:
            mode = "rouge-"+mode
            print("Average {}:\tP: {: 2.2f}\tR: {: 2.2f}\tF1: {: 2.2f}".format(mode,
                   (summary[mode+'-p'][0]/summary[mode+'-p'][1])*100,
                   (summary[mode+'-r'][0]/summary[mode+'-r'][1])*100,
                   (summary[mode+'-f'][0]/summary[mode+'-f'][1])*100))

        # Write results to file
        with open(res_out_file,"w") as f:
            for metric_res in metric_results:
                f.write(json.dumps(metric_res)+"\n")
            f.close()