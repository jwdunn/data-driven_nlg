# Linguistic realiser
# Jarryd Dunn
# 2019/07/04

import tensorflow as tf
import numpy as np
import embedWords
import json

def embed_sentence(sentence, vectors, word2int):
    embed_sentence = []

    for word in sentence:
        embed_sentence.append(vectors[word2int[word]])

    return embed_sentence

#def get_embeddings(sentences, vocab_size, word2int, window, dimensions, num_sampled, batch_size)

if __name__ == "__main__":
    training_filename = "datasets/africanWildDog_tokens.txt"
    label_filename = "datasets/africanWildDog_delex.json"
    sentences = []
    words = []
    vocab_size = -1

    # Read in tokens (training input)
    with open(training_filename,'r') as f:
        raw_text = f.readlines()
        vocab_size = 0

        # Split each sentence into an array
        for sentence in raw_text:
            sentences.append(sentence.strip().split("|"))
            words += sentences[-1]
        f.close()

    # Read in labels
    raw_json = []
    json_text = [] # sentences
    label_words = [] # individual words
    with open(label_filename,'r') as f:
        raw_json_text = f.readlines()
        for line in raw_json_text:
            raw_json.append(json.loads(line))
            json_text += raw_json[-1]["TEXT"]
            for sentence in raw_json[-1]["TEXT"]:
                for word in sentence:
                    label_words.append(word)
    """
    # Calculate word dictionaries
    word_set = set(words)
    word2int, int2word = embedWords.wordIntDict(word_set)
    label_words = set(label_words)
    word2int_label, int2word_label = embedWords.wordIntDict(label_words)
    label_size = len(label_words)

    # Calculate word embeddings
    vocab_size = len(word_set)
    TRAINING_WINDOW = 2 # Skipgram window
    TRAINING_EMBEDDING_DIM = 8 # Number of dimensions for embedding vector
    TRAINING_NUM_SAMPLED = 4
    TRAINING_BATCH_SIZE=10

    LABEL_WINDOW = 2
    LABEL_EMBEDDING_DIM = 8 # Number of dimensions for embedding vector
    LABEL_NUM_SAMPLED = 4
    LABEL_BATCH_SIZE=10
    # Contains skipgram pairs
    data = embedWords.get_skipgram_pairs(sentences, TRAINING_WINDOW)
    
    # Contains word embeddings
    vectors = embedWords.learn_embeddings(data, word2int, vocab_size, TRAINING_BATCH_SIZE, TRAINING_EMBEDDING_DIM, TRAINING_NUM_SAMPLED)

    embed_sentence(sentences[0], vectors, word2int)

    embedWords.graph_embeddings3d(list(word_set), vectors, word2int)

    #label_data = embedWords.get_skipgram_pairs(json_text, LABEL_WINDOW)
    #label_vectors = embedWords.learn_embeddings(label_data, word2int_label, label_size, LABEL_BATCH_SIZE, LABEL_EMBEDDING_DIM, LABEL_NUM_SAMPLED)
    """
    #Get  arrays for training
    token_sentences = []
    for line in raw_text:
        line = line.split("<end>")
        for i in range(len(line)):
            token_sentences.append(line[i].split("|")[:-1])
    token_sentences = token_sentences[:-1]

    print(len(token_sentences), len(json_text))

    for i in range(len(token_sentences)):
        token_sentence = token_sentences[i]
        for token in set(token_sentence):
            if token_sentence.count(token) != json_text[i].count(token):
                print(i,token_sentence)
        
    
