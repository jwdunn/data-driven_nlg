#!/bin/bash

onmt-main infer \
	--auto_config --config data.yml \
	--features_file $1 \
	--predictions_file $2 \
	--model_type NMTMedium
#	--checkpoint_path averageCheckpoint/model.ckpt-10000 \ Need path for averages