#!/bin/bash

# Takes in files for training source, vocab_size, training_target, vocab_size

# Preprocess vocab
onmt-build-vocab --size $2 --save_vocab datasets/src-vocab_person.txt $1
onmt-build-vocab --size $4 --save_vocab datasets/tgt-vocab_person.txt $3

# Train and evaluate model
onmt-main train_and_eval --model_type NMTMedium --auto_config --config $5