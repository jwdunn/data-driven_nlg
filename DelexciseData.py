#Delexicalise json dataset
#Jarryd Dunn
#2019/06/15

import json
import re

class DelexciseData(object):
    '''
    Reads in and delexicalises a file of JSON objects
    '''
    def __init__(self, filename_prefix, cutoff=1):
        self.file_prefix = filename_prefix
        self.json_objects = []
        self.cutoff = cutoff

        
    
    def reverseKeys(self, obj):
        """
        Swap keys and values in dictionary.
        :param obj dictionary loaded from a JSON object
        :return: dictionary mapping slot-values to slot-keys
        """
        revDict = {}
        for key in obj.keys():
            #new_key = re.sub('\s','_',key) # Replace whitespace with _
            #new_keys.append((key,new_key))
            #key=new_key
            if type(obj[key])==str:
                revDict[obj[key]]=key
            elif key!="TEXT":
                values = obj[key]
                for value in values:
                    for item in value.items():
                        if item[0] == "mainsnak":
                            revDict[item[1].strip(".,?!")]=key
                            #print("%s -> %s"%(key,item[1]))
                        else:
                            for qual_item in item[1].items():
                                #print(qual,qual_item[1])
                                for qual in qual_item[1]:
                                    revDict[qual]=key+"|"+qual_item[0]

        return revDict

    def getWords(self, text, words=[]):
        """
        Returns an array containing one of each word present in the text.
        :param text: array containing words from text as elements.
        :param words: array containing words
        :return: array containing a single instance of each word in the text.
        """
        for word in text:
            word = word.strip(".,?!")
            if word not in words:
                words.append(word)
        return words

    def repSpace(self, string):
        """
        Replace a space with _
        :param string: input string
        :return: string with spaces replaces with underscores
        """
        return re.sub('\s','_',string)

    def addSpace(self, string):
        """
        Replace underscores with spaces
        :param string: input string
        :return: string with underscores replaced with spaces
        """
        return re.sub("NAMEID","Name_ID",re.sub("_"," ",re.sub("Name_ID","NAMEID",string)))

    def delexicalise(self, obj, cutoff=0):
        """
        Replace values in text with the corresponding keys.
        :param obj: dictionary constructed from a JSON object for one of the entries.
        :param cutoff: int minimum number of slot-values a sentence needs to contain to be included.
        :return: string containing only the tokens in the sequence they appear in the text.
        """
        end = ['.','?','!',',']
        text = obj["TEXT"]
        new_text = []
        revDict = self.reverseKeys(obj)
        tokenText = []
        #for item in revDict.items():
        #    print(item[0],item[1])
        for i in range(len(text)):
            newline=""
            hasToken=False
            for j in range(len(text[i])):
                word=text[i][j]
                if word in revDict.keys():
                    obj["TEXT"][i][j] = self.repSpace(revDict[word])
                    newline+=self.repSpace(obj["TEXT"][i][j])+" "
                    hasToken=True
            if hasToken: # Prevent empty sentences being added
                if len(newline.split())>=cutoff:
                    newline+="<end>"
                    tokenText.append(newline)
                    new_text.append(text[i])
                
        obj["TEXT"]=new_text # Only include sentences with tokens
        return tokenText

    def print2dArray(self, text):
        """
        print text section of JSON object
        :param text: reference text from JSON object
        """
        for line in text:
            print(" ".join(line))

    def loadData(self, filename):
        """
        Read data from file
        :param filename: string the name of the file containing JSON entries from the data set
        """
        with open("%s.json"%(filename),"r") as f:
            self.data = f.readlines()
            f.close()
    
    def delexcise(self, verbose=False):
        '''
        Delexicalise data writing the output to filename_trainingToken.txt, filename_sentenceTokens.txt and filename_delex.txt
        :param verbose: boolean True if additional information is required
        '''
        self.loadData(self.file_prefix)
        #Count number of words
        words=[]
        
        print("Delexicalising dataset")
        text = []
        tokenText=[]
        counter =0
        for json_str in self.data:
            json_obj = json.loads(json_str)
            self.json_objects.append(json_obj)
            sentences = json_obj["TEXT"]          
            tokenText.append(self.delexicalise(json_obj, cutoff=self.cutoff))
            for line in sentences:
                words = self.getWords(line,words)
            text += json_obj["TEXT"]
            counter +=1
            if verbose and (counter%1000==0):
                print("Delexcised %d of %d"%(counter, len(self.data)))

        print("Writing to %s_delex"%(self.file_prefix))
        with open("%s_delex.txt"%(self.file_prefix),"w") as f:
            for sentence in text:
                sentence_text = " ".join(sentence)
                f.write(sentence_text+'\n')
            f.close()
        print("Done")

        #Writes a text file filtering everything but the tokens from the text
        with open("%s_trainingTokens.txt"%(self.file_prefix),"w") as f:
            for entity in tokenText:
                for sentence in entity:
                    f.write(sentence+"\n")
            f.close()
        self.num_words = len(words)

        # Writes a text file filtering everything but the tokens from the text in one line
        with open("%s_sentenceTokens.txt" % (self.file_prefix), "w") as f:
            for entity in tokenText:
                for sentence in entity:
                    f.write(sentence + " ")
                f.write("\n")
            f.close()
        self.num_words = len(words)
        print("\nNumber of Words: %d"%(len(words)))

    def numWords(self):
        """
        :return: int number of unique words
        """
        return self.num_words
    '''
    def relexcise(self, index, text):
        """
        Replace token types with token values
        :param index: index of json object
        :param text: string to relexicalise
        :return: relexicalised string
        """
        text = self.addSpace(text).split()
        lookup = self.lookups[index]
        for i in range(len(text)):
            word = text[i]
            if word in lookup.keys():
                value = lookup[word][0] # Get first matching value
                lookup[word] = lookup[word][1:]+[value] # Shuffel values
                text[i] = value
        return " ".join(text)
    '''


if __name__ == "__main__":
    # json_str = '{"Name_ID": "African clawless otter", "taxon rank": [{"mainsnak": "Species"}], "parent taxon": [{"mainsnak": "Aonyx"}], "instance of": [{"mainsnak": "Taxon"}], "gestation period": [{"mainsnak": "63"}], "TEXT": [["the", "African clawless otter", "(", "Aonyx", "capensis", ")", "also", "known", "as", "the", "African clawless otter", "or", "African clawless otter", "is", "the", "second-largest", "freshwater", "species", "of", "otter", "."], ["Aonyx", "capensis", "is", "a", "member", "of", "the", "weasel", "family", "(", "Mustelidae", ")", "and", "of", "the", "order", "Carnivora", "."], ["the", "earliest", "known", "species", "of", "otter", "Potamotherium", "valetoni", "occurred", "in", "the", "upper", "Oligocene", "of", "Europe", ":", "A.", "capensis", "first", "appears", "in", "the", "fossil", "record", "during", "the", "Pleistocene", "."], ["Aonyx", "is", "closely", "related", "to", "the", "extinct", "giant", "Sardinia"], ["mammal species of the world", "lists", "six", "subspecies", "of", "Aonyx", "capensis", ":"], ["however", "some", "authorities", "consider", "the", "Congo/Cameroon clawless otter", "to", "be", "a", "separate", "species", "(", "A.", "congicus", ")", "."], ["despite", "being", "closely", "related", "to", "the", "oriental small-clawed otter", "the", "African clawless otter", "is", "often", "twice", "as", "massive", "as", "that", "relatively", "diminutive", "mustelid", "."], ["gestation", "lasts", "around", "two", "months", "(", "63", "days", ")", "."], ["the", "diet", "of", "Aonyx", "capensis", "primarily", "includes", "water-dwelling", "animals", "such", "as", "crabs", "fish", "frogs", "and", "worms", "."], ["the", "African clawless otter", "spends", "its", "days", "swimming", "and", "catching", "food", "."], ["Aonyx", "specimens", "will", "often", "forage", "in", "man-made", "fisheries", "and", "may", "be", "hunted", "or", "become", "entangled", "in", "nets", "."], ["the", "Otter Trail", "is", "a", "hiking", "trail", "in", "South Africa", "named", "after", "the", "African clawless otter", "which", "is", "found", "in", "this", "area", "."], ["habitat", "selection", "by", "the", "Cape", "clawless", "otter", "(", "Aonyx", "capensis", ")", "in", "rivers", "in", "the", "Western", "Cape", "Province", "South", "Africa", "."], ["foraging", "behaviour", "of", "Cape", "clawless", "otters", "(", "Aonyx", "capensis", ")", "in", "a", "marine", "habitat", "."], ["African clawless otter", "in", "Toledo Zoo", "Ohio", "."]]}'
    fname = "datasets/wiki_person_testing"
    delex = DelexciseData(fname)
    delex.delexcise(verbose=True)