import re


class MeaningRepresentation(object):
    def __init__(self, json_obj):
        '''
        Initialises MeaningRepresentation
        :param json_obj: dictionary representation of a json object
        '''
        self.obj = json_obj
        self.text = json_obj["TEXT"]
        self.tokens = self.getTokens()
        self.lookup = self.getLookup()

    def getTokens(self):
        """
        Find slot types in objects
        :return: list of tokens
        """
        tokens = []
        for key in self.obj.keys():
            val = self.obj[key]
            if key == "TEXT":
                continue
            elif key == "Name_ID":
                tokens.append(key)
                continue
            for entry in val:
                tokens.append(re.sub('\s', '_', key))
                if type(val) != str and "qualifiers" in entry.keys():
                    qal = entry["qualifiers"]
                    for k in qal.keys():
                        tokens.append(re.sub('\s', '_', key + "|" + k))
        #tokens.remove('instance_of') # Since all entries are human
        if 'sex_or_gender' in tokens:
            tokens.remove('sex_or_gender')
        if 'instance_of' in tokens:
            tokens.remove('instance_of')
        #print("Tokens: ", tokens)
        return tokens

    def getLookup(self):
        """
        Generates lookup matching slot-types to slot-values
        :return: dictionary containing slot types and a list of slot values
        """
        lookup = {}
        for key in self.obj.keys():
            if key == "TEXT":
                continue
            if key == "Name_ID":
                lookup[key] = [self.obj[key]]
            else:
                for entry in self.obj[key]:
                    for entryKey in entry.keys():
                        if entryKey == "mainsnak":
                            if key not in lookup.keys():
                                lookup[key] = [entry[entryKey]]
                            else:
                                lookup[key].append(entry[entryKey])
                        else:
                            for qualKey in entry[entryKey]:
                                indexKey = key + "|" + qualKey
                                for qual in entry[entryKey][qualKey]:
                                    if indexKey not in lookup:
                                        lookup[indexKey] = [qual]
                                    else:
                                        lookup[indexKey].append(qual)
        return lookup

    def replaceTokens(self, text):
        """
        Re-lexicalises the text
        :param text: list of words forming delexicalised text
        :return: list of words forming re-lexicalised text
        """
        plain_text = []
        if self.lookup == None:
            self.lookup = self.getLookup()
        for token in text:
            if "_" in token and token != "Name_ID":
                token = re.sub("_", " ", token)
                if token not in self.lookup.keys():
                    token = '<unk>'
            if token in self.lookup.keys():
                plain_text.append(self.lookup[token][0])
                self.lookup[token] = self.rotateArray(self.lookup[token])
            elif token != "<unk>":  # Exclude <unk> tokens
                plain_text.append(token)
        return plain_text

    def rotateArray(self, array):
        """
        Moves the first element of the array to the end
        :param array: An array of 1 or more elements
        :return: The array with the first element in the last position
        """
        if len(array) > 1:
            array = array[1:] + [array[0]]
        return array

    def numUniqueTokens(self):
        """
        Returns the number of slot types
        :return: int The number of slot-types
        """
        return len(self.lookup)

    def numTokens(self):
        """
        Returns the number of slot-values
        :return:  int the number of slot-values
        """
        total = 0
        for token in self.lookup.keys():
            total += len(self.lookup[token])
        return total