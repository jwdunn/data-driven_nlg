# Generate word embdeddings V1.0
# From https://www.tensorflow.org/tutorials/representation/word2vec
# Jarryd Dunn
# 2019/07/15

import tensorflow as tf
import numpy as np
import re
import math
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.manifold import TSNE
from sklearn import preprocessing
from sklearn.decomposition import PCA


def one_hot_vector(data_point_index, vocab_size):
    """
    Generate a one hot vector where the one is at the index position
    """
    
    temp = np.zeros(vocab_size)
    temp[data_point_index] = 1
    return temp

def get_skipgram_pairs(sentences, window):
    """
    Returns a list contianing pairs with each word and the words within
    window characters to the left and right.
    """
    
    data = []
    for sentence in sentences:
        for word_index, word in enumerate(sentence):
            for window_word in sentence[max(word_index - window,0)
                                    :min(word_index+window,len(sentence)+1)]:
                if window_word != word:
                    data.append([word, window_word])
    return data

def get_training_data(data, word2int):
    """
    Takes the data word pairs from the skipgram model and uses turns them
    into one hot vectors so embeddings can be learnt.
    """
    
    x_train = []
    y_train = []

    for data_word in data:
        x_train.append(word2int[data_word[0]])
        y_train.append(word2int[data_word[1]])

    x_train = np.asarray(x_train)
    y_train = np.asarray(y_train)
    return(x_train, y_train)

def wordIntDict(words):
    word2int = {}
    int2word = {}

    for i,word in enumerate(words):
        word2int[word]=i
        int2word[i] = word
    return (word2int, int2word)

def simple_batch(x_train, y_train, size):
    indexes = random.sample(range(0,len(x_train)),size)
    #print(indexes)
    x_batch=[]
    y_batch=[]
    for i in range(size):
        x_batch.append(x_train[i])
        y_batch.append([y_train[i]])
    return (x_batch, y_batch)

def graph_embeddings(words, vectors, word2int):
    fig, ax = plt.subplots()

    model = PCA(n_components=2) #TSNE(n_components=2, random_state=0)
    np.set_printoptions(suppress=True)
    pca_vectors = model.fit_transform(vectors)
    #normalizer = preprocessing.Normalizer()
    norm_vectors = pca_vectors#normalizer.fit_transform(pca_vectors)
    
    for word in words:
        x = norm_vectors[word2int[word]][0]
        y = norm_vectors[word2int[word]][1]
        ax.annotate(word, (x, y))
        ax.plot(x,y,"ob")
    plt.show()
    
def graph_embeddings3D(words, vectors, word2int):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')

    model = PCA(n_components=3) #TSNE(n_components=2, random_state=0)
    np.set_printoptions(suppress=True)
    pca_vectors = model.fit_transform(vectors)
    #normalizer = preprocessing.Normalizer()
    norm_vectors = pca_vectors#normalizer.fit_transform(pca_vectors)
    
    for word in words:
        x = norm_vectors[word2int[word]][0]
        y = norm_vectors[word2int[word]][1]
        z = norm_vectors[word2int[word]][2]
        ax.text(x, y, z, word)
        ax.scatter(x,y,z,c="b")
    plt.show()

def learn_embeddings(model_data, word2int, vocab_size, batch_size=16, embedding_dim=8, num_sampled=16):
    #order data into input label pairs
    x_train, y_train = get_training_data(model_data, word2int)

    embeddings = tf.Variable(tf.random_uniform([vocab_size, embedding_dim], -1.0,1.0))

    weights =  tf.Variable( tf.truncated_normal([vocab_size, embedding_dim],
                                                  stddev=1.0/math.sqrt(embedding_dim)))
    biases = tf.Variable(tf.zeros([vocab_size]))

    # Placeholders for inputs
    train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
    train_labels = tf.placeholder(tf.int32, shape=[batch_size,1])

    embed = tf.nn.embedding_lookup(embeddings, train_inputs)

    loss = tf.reduce_mean(tf.nn.nce_loss(weights=weights,
                     biases=biases,
                     labels=train_labels,
                     inputs=embed,
                     num_sampled=num_sampled,
                     num_classes=vocab_size))

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=1.0).minimize(loss)

    session = tf.Session()
    init = tf.global_variables_initializer()
    session.run(init)

    for i in range(10001):
        x_batch, y_batch = simple_batch(x_train, y_train, batch_size)
        feed_dict = {train_inputs: x_batch, train_labels: y_batch}
        _, cur_loss = session.run([optimizer, loss], feed_dict=feed_dict)
        if i%2000==0:
            print("Step: %d, Loss: %.3f"%(i,cur_loss))

    # Get embeddings
    return session.run(embeddings)
    
if __name__ == "__main__":
    text = 'In the beginning God created the heavens and the earth. \
    Now the earth was formless and empty, darkness was over the surface of the \
    deep, and the Spirit of God was hovering over the waters. \
    And God said, Let there be light, and there was light. \
    God saw that the light was good, and he separated the light from the darkness. \
    God called the light day, and the darkness he called night. And there was \
    evening, and there was morning the first day. \
    And God said, Let there be a vault between the waters to separate water from \
    water .\
    So God made the vault and separated the water under the vault from the water \
    above it. And it was so. God called the vault sky. And there was evening, \
    and there was morning the second day.'.lower()

    words = re.sub('[^a-zA-Z_0-9\s]','',text)
    words = set(words.split())

    word2int, int2word = wordIntDict(words)

    vocab_size = len(words)
    EMBEDDING_DIM  = 5
    WINDOW = 2
    NUM_SAMPLED=16
    BATCH_SIZE=10
    print(vocab_size, EMBEDDING_DIM)


    # split into sentences
    raw_sentences = re.sub('[^a-zA-Z_0-9\s.]','',text).split(".")

    #split sentences into lists
    sentences = []
    for sentence in raw_sentences:
        sentences.append(sentence.split())

    data = get_skipgram_pairs(sentences, WINDOW)

    vectors = learn_embeddings(data, word2int, vocab_size, BATCH_SIZE, EMBEDDING_DIM, NUM_SAMPLED)

    #graph Embeddings
    graph_embeddings(words, vectors, word2int)
