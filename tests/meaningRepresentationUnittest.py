# Unit tests for meaning represenations

import unittest
import json
import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], '..')) # Access classes from files in parent directory
from MeaningRepresentation import MeaningRepresentation as Mr

class MrTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        basepath = os.path.dirname(__file__)
        with open(os.path.join(basepath, "test_mr.json"), 'r') as f:
            cls.obj = json.loads(f.readline())
            f.close()
        cls.mr = Mr(cls.obj)

    def test_tokens(self):
        tokens = ['Name_ID', 'date_of_birth', 'country_of_citizenship', 'participant_of', 'participant_of', 'family_name']
        self.assertListEqual(self.mr.getTokens(), tokens, "Token Lists don't match")

    def test_replaceTokens(self):
        token_text = 'Name_ID family_name date_of_birth participant_of country_of_citizenship participant_of'.split()
        plain_text = ['Daryl Impey', 'Impey', '6 December 1984', 'Tour de France', 'South Africa', '2013 Tour de France']
        self.assertListEqual(self.mr.replaceTokens(token_text), plain_text, "Tokens incorrectly replaced")

    def test_rotateArray(self):
        array = [1,2,3]
        self.assertListEqual(self.mr.rotateArray(array), [2,3,1], 'Array incorrectly rotated')

    def test_unkTokens(self):
        self.assertEqual(self.mr.numUniqueTokens(), 7, 'Should be 7 unique tokens')

    def test_numTokens(self):
        self.assertEqual(self.mr.numTokens(), 8, 'Should be 8 unique tokens')
if __name__ == "__main__":
    unittest.main()
