# Unit tests or delexicaliseDataset
# Jarryd Dunn

import unittest
import json
import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], '..')) # Access classes from files in parent directory
from DelexciseData import DelexciseData

class TestDelex(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        basepath = os.path.dirname(__file__)
        cls.data = DelexciseData(os.path.join(basepath, 'test_mr'))
        cls.data.delexcise()

    def test_init(self):
        self.assertIsNotNone(self.data)

    def test_tokens(self):
        basepath = os.path.dirname(__file__)
        with open(os.path.join(basepath, 'test_mr.json'), 'r') as f:
            obj = json.loads(f.readlines()[0])
        #print(" ".join(self.data.delexicalise(obj,1)))
        self.assertEqual(" ".join(self.data.delexicalise(obj,1)), " ".join(["Name_ID date_of_birth country_of_citizenship <end>", "family_name <end>", "family_name <end>", "family_name <end>", "family_name <end>", "family_name <end>", "family_name <end>", "family_name <end>", "family_name participant_of <end>", "country_of_citizenship <end>", "participant_of <end>", "participant_of <end>"]), "Incorrect tokens")

    def test_numWords(self):
        self.assertEqual(self.data.num_words, 147, "Should be 147")
        

if __name__ == '__main__':
    unittest.main()
