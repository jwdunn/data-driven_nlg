import unittest

import datasetUnittest
import meaningRepresentationUnittest
import sentencePlannerUnittest
from os import getcwd

if __name__ == '__main__':
    print("RUNNER PATH:", getcwd())
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    suite.addTests(loader.loadTestsFromModule(datasetUnittest))
    suite.addTests(loader.loadTestsFromModule(meaningRepresentationUnittest))
    suite.addTests(loader.loadTestsFromModule(sentencePlannerUnittest))

    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
