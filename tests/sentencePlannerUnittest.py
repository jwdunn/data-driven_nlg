# Unit tests for sentence planner
# Jarryd Dunn

import unittest
import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], '..')) # Access classes from files in parent directory
import SentencePlanner as sp

class SentencePlanTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        basepath = os.path.dirname(__file__)
        cls.text = sp.readTokenLines(os.path.join(basepath, 'test_mr'))
        cls.startp = sp.calcStartProb(cls.text)

    def test_startP(self):
        maxP = 0
        maxT = ''
        for token, prob in self.startp.items():
            if prob > maxP:
                maxP = prob
                maxT = token
        self.assertEqual(maxT, 'Name_ID', 'Most probable start should be Name_ID')

if __name__ == '__main__':
    unittest.main()
